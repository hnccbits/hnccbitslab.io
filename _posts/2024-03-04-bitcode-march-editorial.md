---
layout: post
title: BITCode March Round Editorial
subtitle: Editorial

css: /assets/css/style.css
author: Harsh Kumar
tags: [BITCode March, solutions, editorial, competitive programming]
---

**Hi.** Thank you for participating in the BITCode March Round. We hope that you enjoyed the problems we prepared for you. Incase, if you weren’t able to solve them, here are the detailed explanations and solutions with tested code that you can read to figure out what went wrong in the contest.

&nbsp;

Upsolving the problems is the most important thing to do after a contest. Here they are:-

# **Dice Game**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bitcode-march-round/challenges/dice-duel-success)

-  **Author:** [Vaishwanavi Kumari](https://codeforces.com/profile/vaishwanavi_93)

-  **Topics Covered:** math

### **Problem Setter's Solution:**

Explanation:

It was a simple implementation problem
•	There are two numbers x and y
•	If their sum is greater 6 then it is Good otherwise Not Good.

```c++
#include <bits/stdc++.h>
using namespace std;

int main()
{
    
    
        int T; cin>>T;

        while(T--) {

            int x, y; cin>> x >> y;

            if(x + y > 6) {
                cout<<"Good turn!"<<'\n';
            }
            else {
                cout<<"Not a Good turn."<<'\n';
            }
        
        }
    
    return 0;

}

```

# **Helping Linda**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bitcode-march-round/challenges/helping-linda)

-  **Author:** [Amaan](https://codeforces.com/profile/Amaanhussain11)

-  **Topics Covered:** array, bit-manipulation, maths

### **Problem Setter's Solution:**

The idea is to iterate over all the possible pairs and calculate the AND value of those all. and pick the largest value among them.

Follow the steps mentioned below to implement the approach:

•	Declare a variable res to store the final answer.

•	Create a nested loop and traverse all pairs, storing the maximum value of AND of a pair on res.
Return res.

•	Return the final answer as res.


```c++

#include <bits/stdc++.h>
using namespace std;


int maxAND(int arr[], int n)
{
	int res = 0;
	for (int i = 0; i < n; i++)
		for (int j = i + 1; j < n; j++)
			res = max(res, arr[i] & arr[j]);

	return res;
}


int main()
{
	int T; cin>>T;

    while(T--) {
        int n; cin>>n;

        int arr[n]; 

        for(int i = 0; i < n; ++i) {
            cin>>arr[i];
        }

        cout<<maxAND(arr, n)<<'\n';

    }

	return 0;
}


```

# **Vanishing Balls**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bitcode-march-round/challenges/vanishing-balls)

-  **Author:** [Shruti Kedia](https://codeforces.com/profile/Shrutikedia)

-  **Topics Covered:** Stack, Data Structure

### **Problem Setter's Solution:**


•	The problem  at using a stack data structure to handle the ball removal process efficiently.

•	We iterate through each ball and push it into the stack.

•	While pushing, we check if the top of the stack has the same value as the current ball.

•	If it does, it means they form a sequence, so we increase the count for that value.

•	If the count becomes equal to the value itself, it means k balls with k written on them line up, and they will all disappear. So, we pop them from the stack.

•	At each step, we output the size of the stack, which represents the number of balls remaining.

```c++
#include<bits/stdc++.h>

using namespace std;

int main() {

    ios::sync_with_stdio(0);cin.tie(0);

    int n; cin>>n;

    int a[n];

    for(int i = 0; i < n; ++i) cin>>a[i];

    stack<pair<int, int>> st;

    for(int i = 0; i < n; ++i) {
        if(!st.empty() && st.top().first == a[i]) {
            st.push({a[i], st.top().second + 1});
        }
        else {
            st.push({a[i], 1});
        }

        if(st.top().first == st.top().second) {
            int k = a[i];

            while(k--) st.pop();
        }

        cout<<st.size()<<'\n';
    }

    return 0;
}

```

# **Breaking Bad**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bitcode-march-round/challenges/bad-breaking/problem)

-  **Author:** [Amaan Hussain](https://codeforces.com/profile/Amaanhussain11)

-  **Topics Covered:** Dynamic Programming

### **Problem Setter's Solution:**

•	Initialize a DP table dp[] with size k+1, where k is the number of lines in the letter. Set dp[0] = 0 as the base case.

•	Iterate through each line length i from 1 to k.

•	For each line length i, iterate through each type of pencil. For each pencil, if the breaking point of the pencil is less than or equal to the current line length i, update the dp[i] with the minimum cost between the current dp[i] and dp[i - breaking_point_of_current_pencil] + cost_of_current_pencil.

•	After iterating through all line lengths and pencils, the value of dp[k] will represent the minimum cost to write the entire letter.

```c++
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

int main() {
    int t;
    cin >> t;
    
    while (t--) {
        int k;
        cin >> k;
        
        int p;
        cin >> p;
        
        vector<pair<int, int>> pencils;
        for (int i = 0; i < p; ++i) {
            int bp, cost;
            cin >> bp >> cost;
            pencils.push_back({bp, cost});
        }
        
        
        vector<long long> dp(k + 1, INT_MAX);
        dp[0] = 0;
        
        for (int i = 1; i <= k; ++i) {
            for (int j = 0; j < p; ++j) {
                if (pencils[j].first <= i) {
                    dp[i] = min(dp[i], dp[i - pencils[j].first] + pencils[j].second);
                }
            }
        }
        
        cout << dp[k] << endl;
    }
    
    return 0;
}
```

# **Find Minimal**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bitcode-march-round/challenges/yet-another-subsequence-problem-1)

-  **Author:** [Harsh Kumar](https://codeforces.com/profile/harsh_1806) 

-  **Topics Covered:** Binary Search, Bit Manipulation

### **Problem Setter's Solution:**

Key Idea:

Binary search over the answer and check if given x, it is possible to form a subsequence of length at least k  such that either all elements at odd indices or even indices are ≤ x.

Solution:

Let us binary search over the answer and fix if the answer comes from elements at odd or even indices in the subsequence. Suppose we want to find if there exists a subsequence of length at least k such that the elements at odd indices are ≤ x. We will construct the subsequence greedily.

Let's iterate on the array from left to right. Suppose we are at index i in the array and the current length of the subsequence formed is l. If l is odd, the next added element would be at an even index. In this case, we do not care about what this element is as we only want elements at odd indices to be ≤ x. So, in this case, we add ai to the subsequence. If l is even, then the next added element would be at an odd index, so, it must be ≤x. If ai≤x, we can add ai to the subsequence, otherwise we do not add ai to the subsequence and continue to the next element in a.

Note that we can do a similar greedy construction for elements at even indices. If the length of the subsequence formed is ≥ k (either by construction from odd indices or even indices), then the answer can be equal to x
 and we can reduce the upper bound of the binary search otherwise we increase the lower bound.

Time Complexity - O(n⋅log2(Ai)) or O(n⋅log2(n))
```c++
#include <bits/stdc++.h>
using namespace std;
 
#define endl "\n"
#define int long long

const int N = 2e5 + 5;

int n, k;
int a[N];

bool check(int x, int cur)
{
    int ans = 0;
    for(int i = 1; i <= n; i++)
    {
        if(!cur)
        {
            ans++;
            cur ^= 1;
        }
        else
        {
            if(a[i] <= x)
            {
                ans++;
                cur ^= 1;
            }
        }
    }
    return ans >= k;
}

int binsearch(int lo, int hi)
{
    while(lo < hi)
    {
        int mid = (lo + hi) / 2;
        if(check(mid, 0) || check(mid, 1))
            hi = mid;
        else
            lo = mid + 1;
    }
    return lo;
}

int32_t main()
{
    ios::sync_with_stdio(0); cin.tie(0);
    
    cin >> n >> k;

    for(int i = 1; i <= n; i++)
        cin >> a[i];

    int ans = binsearch(1, 1e9);

    cout << ans;
    
    return 0;
}
```

Happy Learning!!!

<!-- WRITTEN BY: Harsh Kumar -->

<!-- Tags: BITCode March  solutions   editorial   competitive programming -->