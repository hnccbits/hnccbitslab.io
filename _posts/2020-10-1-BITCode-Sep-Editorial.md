---
layout: post
title: BITCode September Round Editorial
subtitle: Editorial
# bigimg: 
#   - "https://cdn.pixabay.com/photo/2016/11/29/05/55/asphalt-1867667_960_720.jpg" : "Contest"

css: /assets/css/styles.css
author: Anjani Kumar

tags : [BITCode September, solutions, editorial, competitive programming]
---

Hi. Thank you for participating in the BITCode September Round. We hope that you enjoyed the problems we prepared for you. Incase, if you weren't
able to solve them, here are the solutions with tested code that you can read to figure out what went wrong in the contest.
Upsolving the problems is the most important thing to do after a contest. Here they are:-

* [Chef and Lines](https://discuss.codechef.com/t/editorial-ttiline/78788)
* [Change Directory](https://discuss.codechef.com/t/editorial-chdir/78782)
* [Chef and Covid](https://discuss.codechef.com/t/editorial-chfcovid/78786)
* [Chef and his garden](https://discuss.codechef.com/t/editorial-chefgdn/78785)
* [Not Again Binod!](https://discuss.codechef.com/t/editorial-nobin/78790)

Thank you
Happy Learning!!!

