---
layout: post
title: "HnCC Experience (1)"
subtitle: Journey to Tech
author: Anjali Kumari - Chem 2k19
tags : [2k19, Intro, Experience, Development, CP, Open Source, HacktoberFest, 2020]
---

Hello everyone, ***I am Anjali from 2k19, Chemical Engineering***, not exactly.....
I started my journey to the world of tech only after getting indulged in HnCC. To be a part of this prestigious club was a passion of mine when I was new to the college.

I was not interested & satisfied with my branch and I didn't even know how can I move towards my goal which was also hidden in the clouds, that time.

The day I got the news that `"Anjali, You are inducted in HnCC"`, it seemed cloud breaking, and all things got cleared to me then.

***1. Android Development***

I started my tech journey first with `Native Android Development`, as a beginner I thought of choosing such a platform in which I can visualize my work and that suited my need handily. 

During the journey I learned many things and my curiosity to learn more got its height. I started making APIs use it in my Android projects. 
Explored the concept of `JSON Parsing`, `Volley`, `Room database`, `Firebase`, and many more. 

One of the apps of mine that I made using the concepts learned:-

`BIT NETWORK` -----> It is an open-source project made to connect `Alumni`, `Teachers`, `Non-Teachers`, `Seniors` all together via this platform, they can `post contents`, `comment`, `chat` with anyone and a much more with superb frontend. Some glimpse of it :

<p align="center">
<img src="https://imgur.com/bMOe593.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
<img src="https://imgur.com/H6VxeBM.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
<img src="https://imgur.com/XqGViLG.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
</p>

<p align="center">
<img src="https://imgur.com/wGPUr9j.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
<img src="https://imgur.com/FIOLAX2.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
</p>

<p align="center">
<img src="https://imgur.com/pDXX3gp.jpg" width="200" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
</p>

check out this project at my [github bit network][github bit network]

Check all of my Open Source Projects at [GitHub](https://github.com/anjali1361 "Github Link")

***2. Competitive Programming***

While leading with Android Dev, I felt the importance of Competitve Programming(CP) including Data Structure and Algorithms. This is the base without which we can't get success in any of the tech stacks, I planned to start this next. 

Initially I didn't know, where to start. But, Under the guidance of my seniors, I started CP, it seemed a bit difficult to me at that time but something comes to you after a long struggle and later on, I found myself in the track of CP, and it is quite interesting. 

My Journey Of CP would be revealed [here.](https://github.com/anjali1361/Hackerrank_Solution "Solution to some problems")

***3. Backend Web Development***

Then I made Web Projects too to sharpen my knowledge in `Clouds`, `Networking`, `IP Address`, `Port` and deep-dived into it as I found it interesting in this journey, I learned `Nodejs`, and used some of its packages and frameworks.

I believe anything that has been learned theoretically is not much efficient than using this knowledge to make a real-world project.

 Here are some of my web apps which was made using `Nodejs and Express` with `Express-generator` , namely [EMS][EMS] & [PMS][PMS].

<br>
<p align="center">
<img src="https://imgur.com/oE1XaiK.jpg" align="center" width ="500" height="300"> &nbsp; &nbsp; &nbsp; &nbsp;
</p>
<br>

***4. Journey To Open Source, THE HACKTOBERFEST***

Wait, It was not all about, after all this, in the starting of October, I got the real idea behind `Open Source` & it's usage. Contributing to it I realized that this will give me the skills to present my application effectively or can say `HOW TO GET YOUR APPS TO PRODUCTION LEVEL`.

I tried to participate in each event organized in `Hacktoberfest` and made my `4PRs` by late night on October 1st, I didn't know about my labor would be right back to me, and yes......

`I GOT MY T-SHIRT FOR MY 4 PRs THAT GOT MERGED`. Not only that, I got bundles of stickers and a `Dev Badge` also.

 I started following some experts in the field of my interest at [Dev Post][Dev Post] and started to participate in `Hackathons` and `Projects`

```Some Achievements Of HACKTOBERFEST```

<br>
<br>
<p align="center">

<img src="https://imgur.com/P2SkUKb.jpg" align="center" width ="200" height="200"> &nbsp; &nbsp; &nbsp; &nbsp;
<img src="https://imgur.com/s38M7HZ.jpg" align="center" width="200" height="200"> &nbsp; &nbsp; &nbsp; &nbsp;
</p>

`And the journey continues..........`

[github bit network]: https://github.com/anjali1361/Bit-Network
[Dev Post]: https://devpost.com/anjali1361
[EMS]: https://github.com/anjali1361/EMS
[PMS]: https://github.com/anjali1361/PMS
