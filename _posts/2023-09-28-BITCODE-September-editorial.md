---
layout: post
title: BITCode September Round Editorial
subtitle: Editorial

css: /assets/css/style.css
author: Anjali Barnwal
tags: [BITCode August, solutions, editorial, competitive programming]
---

**Hi.** Thank you for participating in the BITCode September Round. We hope that you enjoyed the problems we prepared for you. Incase, if you weren’t able to solve them, here are the detailed explanations and solutions with tested code that you can read to figure out what went wrong in the contest.

&nbsp;

Upsolving the problems is the most important thing to do after a contest. Here they are:-

# **Largest number**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bit-code-september/challenges/largest-number-35-1)

-  **Author:** [Saurabh](https://codeforces.com/profile/chhotta_don)

-  **Topics Covered:** String, math

### **Problem Setter's Solution:**

Explanation:

To find the largest number possible after insertion of the digit, d
•	We’ll check all the digits present in all the indexes one by one.
•	We’ll compare those digits with the given digit, d and as soon as we’ll get the digit which is smaller than d, we’ll insert d before that digit. Or else in the end of the number.
•	s[i]>=d. As soon as this condition is false or the line ends, we’ll insert d.


```c++
#include <bits/stdc++.h>
using namespace std;

void insert_digit(int n, int d, string s)
{
    for (int i = 0; i < n; i++)
    {
        if (s[i] - '0' >= d)
            cout << s[i];
        else
        {
            cout << d;
            for (int j = i; j < n; j++)
                cout << s[j];
            cout << endl;
            return;
        }
    }
    cout << d << endl;
    return;
}
int main()
{
    
    
        int n, d;
        string s;
        cin >> n >> d >> s;
        insert_digit(n, d, s);
    
}

```

# **Win the Race**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bit-code-september/challenges/win-the-race)

-  **Author:** [Anuj](https://codeforces.com/profile/anujk2k2)

-  **Topics Covered:** tag array maths

### **Problem Setter's Solution:**
1. Input: The program takes the following input:
   - `n`: The number of races.
   - `binod_times`: An array representing the time taken by Binod in each race.
   - `ramesh_times`: An array representing the time taken by Ramesh in each race.

2. Initialize Points: Initialize two variables, `binod_points` and `ramesh_points`, to zero. These variables will be used to keep track of the total points earned by Binod and Ramesh, respectively.

3. Calculate Points for Each Race
   - For each race (from 1 to n):
     - Calculate the time difference between Binod and Ramesh in that race (Binod time - Ramesh time).
     - If the difference is positive, it means Binod was faster, so add this difference to Binod's total points (`binod_points += difference`).
     - If the difference is negative, it means Ramesh was faster, so add the absolute difference to Ramesh's total points (`ramesh_points += abs(difference)`).

4. Determine the Winner
   - After calculating the points for all races, compare `binod_points` and `ramesh_points`.
   - If `binod_points` is greater than `ramesh_points`, Binod is the winner.
   - If `ramesh_points` is greater than `binod_points`, Ramesh is the winner.
   - If `binod_points` and `ramesh_points` are equal, it's a tie.

5. Output the Result
   - Print the name of the winner (either "Binod" or "Ramesh") or "Tie" if the points are equal.

```c++
#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;

    vector<int> binod_times(n);
    vector<int> ramesh_times(n);

    // Read input for Binod's times
    for (int i = 0; i < n; i++) {
        cin >> binod_times[i];
    }

    // Read input for Ramesh's times
    for (int i = 0; i < n; i++) {
        cin >> ramesh_times[i];
    }

    int binod_points = 0;
    int ramesh_points = 0;

    // Calculate points for each race and update total points
    for (int i = 0; i < n; i++) {
        int difference = binod_times[i] - ramesh_times[i];
        if (difference > 0) {
            binod_points += difference;
        } else if (difference < 0) {
            ramesh_points += abs(difference);
        }
    }

    // Determine the winner or declare a tie
    if (binod_points > ramesh_points) {
        cout << "Binod" << endl;
    } else if (ramesh_points > binod_points) {
        cout << "Ramesh" << endl;
    } else {
        cout << "Tie" << endl;
    }

    return 0;
}
```

# **Computational infrastructure**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bit-code-september/challenges/computational-infrastructure)

-  **Author:** [Ritik Raj Pandey](https://codeforces.com/profile/Ritik_1p)

-  **Topics Covered:** Binary search

### **Problem Setter's Solution:**
In this problem, you're given n nodes and asked the minimum time these
nodes need to work to create  t  products such that the i-th node
creates a product in ki time.

Observe that the time needed to create at least x products is monotonic. In
other words, if the given machines can create x products in y1 time, then
the same machines can create at least x products in y2 > y1 time. Using
this property, we can binary search over the answer. 

For some value we use binary search for, ans, we are left with the task of
checking whether we can create t products in ans time. To do this, observe
that it's optimal for every node to work simultaneously. Then, in ans time,
node i can create [ans/ki]  products.

Overall, the  n machines can create  ∑ n [ans/ki] products. 
                                                                 i=1
if this sum>=t ,then ans is valid. 
```c++
#include <bits/stdc++.h>
 
using namespace std;
typedef long long ll;
const int maxN = 2e5;
 
int N;
ll T, cnt, k[maxN];
 
bool check(ll t){
    cnt = 0;
    for(int i = 0; i < N; i++){
        cnt += t/k[i];
        if(cnt >= T)
            return true;
    }
    return false;
}
 
int main(){
    scanf("%d %lld", &N, &T);
    for(int i = 0; i < N; i++)
        scanf("%lld", &k[i]);
 
    ll lo = 0, hi = 1e18;
    while(lo <= hi){
        ll mid = lo + (hi-lo)/2;
        if(check(mid))  hi = mid-1;
        else            lo = mid+1;
    }
    printf("%lld\n", lo);
}
```

# **Sweets corner**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bit-code-september/challenges/sweets-corner)

-  **Author:** [Mohit Pandey](https://codeforces.com/profile/pandeymohit215)

-  **Topics Covered:** Tag-segment tree

### **Problem Setter's Solution:**
if the max possible different number of sweets
  was limited to some smaller amount(instead of a billion)
  we will be able to solve it.

So try solving the problem under the constraint that p,a,b <= 10^7.
Now the problem is much easier if I maintain the number of  sweets with a given quantity, 
let us define freq[i] : number of sweets with the quantity i
We may now build a range sum query segment tree on this array and to answer
 a query we simply calculate the sum of the range [a,b].

For updating the quantity of a sweet from x to y, we do the point updates
 freq[x] -= 1 and freq[x] += 1 because now 1 less sweet has quantity x and 1
  more sweet has the quantity y.

  But the problem is not solved, since we needed to do this for max possible salary = 1billion,
   but now we know how to do it for 10^7.
   We know 
   10^9 = 10^7 * 10^2

   So lets group the quantities into 10^7 buckets and each bucket represent a range of 100 different
    contiguous quantity values. The 0th bucket represents quantity from
    1 to 100 and ith bucket represents the quantity from (i)*100 + 1 to (i+1)*100.

These buckets will store aggregated number of sweets that have quantity in the given range 
represented by the bucket.

Now for query [a,b] : all the buckets that are entirely in the range [a,b] their aggregate values 
should be taken and summed up and for the 2 partial buckets(or 1) not entirely included in the range
 [a,b] we shall do a brute force.
 So build a segment tree over the buckets and calculate the sum over all completely included buckets 
 in the range [a,b]. For remaining partially included buckets do a brute force(actually iterate over approx
  100 possible values and choose to include 
 those which are required by a particular query).

```c++
#include<bits/stdc++.h>
#define fast_io ios_base::sync_with_stdio(false);cin.tie(NULL)
#define ll long long
using namespace std;
#define left(i) (2*i + 1)
#define right(i) (2*i + 2)
#define parent(i) ((i-1)/2)
#include <vector>
 
template<class T>
class SegmentTree
{
    public:
        SegmentTree(std::vector<T> data, T value, T (*combine)(T obj1, T obj2));
        SegmentTree(T ar[], int n, T value, T (*combine)(T obj1, T obj2));
        T query(int l, int r);
        void update(int idx, T val);
        //TODO lazy propagation
    private:
        T *tree;
        void buildTree(std::vector<T> data);
        int segTreeSize;
        T valueForExtraNodes;
        T (*combine)(T obj1, T obj2);
        int calculateSize(int n);
        T queryHelper(int l,int r, int st, int ed, int node);
};
 
template<class T> SegmentTree<T>::SegmentTree(std::vector<T> data,
                                                T value, T (*combine)(T obj1, T obj2))
{
   this->combine = combine;
   valueForExtraNodes = value;
   segTreeSize = calculateSize(data.size());
   buildTree(data);
}
 
template<class T> SegmentTree<T>::SegmentTree(T ar[], int n,
                                            T value, T (*combine)(T obj1, T obj2))
{
   this->combine = combine;
   valueForExtraNodes = value;
   segTreeSize = calculateSize(n);
 
   std::vector<T> data;
   for(int i = 0; i < n; i++)
         data.push_back(ar[i]);
 
   buildTree(data);
}
 
 
template<class T> int SegmentTree<T>::calculateSize(int n)
{
    int pow2 = 1;
    while( pow2 < n)
    {
        pow2 = pow2 << 1;
    }
    return 2*pow2 - 1;
}
 
template<class T> T SegmentTree<T>::query(int l, int r)
{
    int st = 0, ed = segTreeSize/2;
    return queryHelper(l, r, st, ed, 0);
}
 
template<class T> T SegmentTree<T>::queryHelper(int l,int r, int st, int ed, int node)
{
    if( (r < st) || (l > ed) || (l > r) )
        return valueForExtraNodes;
    if(st >= l && ed <= r)
        return tree[node];
    T leftVal = queryHelper(l, r, st, (st + ed)/2, left(node));
    T rightVal = queryHelper(l, r, (st+ed)/2 + 1, ed, right(node));
    return combine(leftVal, rightVal);
}
 
template<class T> void SegmentTree<T>::buildTree(std::vector<T> data)
{
   int n = data.size();
   tree = new T[segTreeSize];
   int extraNodes = (segTreeSize/2 + 1) - n;
   for(int i = segTreeSize - 1; i >= 0; i--)
   {
       if(extraNodes>0)
           {
               tree[i] = valueForExtraNodes;
               extraNodes--;
           }
       else if(n>0)
           {
               tree[i] = data[n-1];
               n--;
           }
       else
           tree[i] = combine(tree[left(i)], tree[right(i)]);
   }
}
 
template<class T> void SegmentTree<T>::update(int idx, T val)
{
    int segTreeIdx = (segTreeSize/2) + idx;
    tree[segTreeIdx] = val;
    while(parent(segTreeIdx) >= 0)
    {
        segTreeIdx = parent(segTreeIdx);
        if(right(segTreeIdx) < segTreeSize)
          tree[segTreeIdx] = combine(tree[left(segTreeIdx)], tree[right(segTreeIdx)]);
        if(segTreeIdx == 0)
            break;
    }
}
 
// returns number of employees with salaries between lo and hi
int calc(int lo, int hi, map<int,int>& sal2freq)
{
    int res = 0;
    auto it = sal2freq.lower_bound(lo);
    while(it != sal2freq.end() && it->first <= hi){
        res += it->second;
        it++;
    }
    return res;
}
//returns the bucket number to which x belongs
int bucket_no(int x)
{
    // 1-100 in block 0 but 100/100 = 1
    if(x % 100 == 0)
        x--;
    return (x / 100);
}
 
int sum(int x,int y) { return x+y; }
int main()
{
    fast_io;
    int n,q;
    cin >> n >> q;
 
    vector < int > employee(n);
    vector< int > buckets(10000000, 0);
 
    // salary = key, number of employees with this salary = value
    map < int, int> salary2freq;
 
    for(int  i = 0 ; i < n; i++)
    {
        int salary;
        cin >> salary;
 
        employee[i] = salary;
        salary2freq[salary]++;
 
        buckets[bucket_no(salary)]++;
    }
    SegmentTree < int > rangeSumQueries(buckets, 0, sum);
 
    while(q--)
    {
        char ch;
        cin >> ch;
        if(ch == '!')
        {
            int k,x;
            cin >> k >> x;
            int prev_salary = employee[k-1];
            employee[k-1] = x;
 
            int prev_bucket = bucket_no(prev_salary);
            int new_bucket = bucket_no(x);
 
            buckets[prev_bucket]--;
            buckets[new_bucket]++;
            salary2freq[prev_salary]--;
            salary2freq[x]++;
 
            rangeSumQueries.update(prev_bucket, buckets[prev_bucket]);
            rangeSumQueries.update(new_bucket, buckets[new_bucket]);
        }
        else
        {
            int a,b;
            cin >> a >> b;
 
            int lbucket = bucket_no(a);
            int rbucket = bucket_no(b);
 
            int ans = calc(a, min((lbucket+1)*100, b), salary2freq);
            ans  = ans + ((lbucket!=rbucket) ? calc(max(a, rbucket*100 + 1), b, salary2freq) : 0);
            ans  = ans + rangeSumQueries.query(lbucket+1, rbucket-1);
 
            cout << ans << '\n';
        }
    }
 
    return 0;
}
```

# **Find Minimal**

-  **Problem Link:** [Click Here](https://www.hackerrank.com/contests/bit-code-september/challenges/find-minimal)

-  **Author:** [Abhishek Mondal](https://codeforces.com/profile/Abhishek49966) 

-  **Topics Covered:** Dp

### **Problem Setter's Solution:**
We can add an camera(x=0,s=0)
. It will not modifiy the answer, because it would be 
non-optimal to increase the scope of this camera.

Let dp[x]
 be the minimum cost to cover all positions from x
 to m
 inclusive, knowing that position x
 is covered. We compute dp
 in decreasing order of x
.

Base case is dp[m]:=0
.

The default transition is dp[x]:=(m−x)
.

If position x+1
 is initially covered, dp[x]:=dp[x]+1
Otherwise, let's consider all cameras and their initial intervals [li;ri]
. If x<li
, let u=(li−x−1)
, then a possible transition is dp[x]:=u+dpmin(m,ri+u)
.

We take the minimum of all these transitions. Note that we 
always extend intervals as less as possible, but it's optimal because :

If after using this interval i
, we use another interval j
 (at the right of i
), the time spent to extend i
 could have been used to extend j
 instead, which will be more optimal.
If i
 was the last interval used, we don't care because the default
  transition will take care of this case.
The final answer will be dp[0]
.

There are O(m)
 states and O(n)
 transitions, hence final complexity is O(nm)
 with very low constant. O(n*n*m)
 can also get AC because of very low constant.
```c++
 #include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
 
struct Antenna
{
    int iniLeft, iniRight;
};
 
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
 
    int nbAntennas, totLen;
    cin >> nbAntennas >> totLen;
    vector<Antenna> ants(nbAntennas);
 
    for (int iAnt = 0; iAnt < nbAntennas; ++iAnt) {
        int center, iniScope;
        cin >> center >> iniScope;
        ants[iAnt].iniLeft = max(0, center - iniScope);
        ants[iAnt].iniRight = min(totLen, center + iniScope);
    }
 
    vector<int> minCost(totLen+1);
    minCost[totLen] = 0;
 
    for (int pos = totLen-1; pos >= 0; --pos) {
        minCost[pos] = (totLen - pos);
 
        for (int iAnt = 0; iAnt < nbAntennas; ++iAnt) {
            int left = ants[iAnt].iniLeft, right = ants[iAnt].iniRight;
 
            if (left <= pos+1 && pos+1 <= right) {
                minCost[pos] = minCost[pos+1];
                break;
            }
            
            if (pos < left) {
                int accessCost = (left - pos - 1);
                int nextPos = min(totLen, right + accessCost);
                minCost[pos] = min(minCost[pos], accessCost + minCost[nextPos]);
            }
        }
    }
 
    cout << minCost[0] << "\n";
 
    return 0;
}
```

Happy Learning!!!

<!-- WRITTEN BY: Anjali Barnwal -->

<!-- Tags: BITCode September  solutions   editorial   competitive programming -->