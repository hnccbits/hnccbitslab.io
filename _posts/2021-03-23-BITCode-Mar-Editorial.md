---
layout: post
title: BITCode March Round Editorial
subtitle: Editorial
# bigimg: 
#   - "https://cdn.pixabay.com/photo/2016/11/29/05/55/asphalt-1867667_960_720.jpg" : "Contest"

css: /assets/css/styles.css
author: Abhay Tiwari, Adarsh Kumar Sinha, Hanzala Sohrab

tags : [BITCode March, solutions, editorial, competitive programming]
---

Hi. Thank you for participating in the BITCode March Round. We hope that you enjoyed the problems we prepared for you. We would like to apologize for the late editorial. This time we reduced the difficulty. Incase, if you weren't able to solve them, here are the solutions with tested code that you can read to figure out what went wrong in the contest.

Upsolving the problems is the most important thing to do after a contest. Here they are:-

* [Partition Problem](https://discuss.codechef.com/t/parttn-editorial/87091)
* [Corona and Planet G](https://discuss.codechef.com/t/corg-editorial/87095)
* [Binod and Braces](https://discuss.codechef.com/t/binbr-editorial/87094)
* [Card Game](https://discuss.codechef.com/t/cardg-editorial/87092)


Thank you
Happy Learning!!!